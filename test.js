const { JSDOM } = require('jsdom');
const assert = require('assert').strict;
const Util = require('./src');

const jsdom = new JSDOM('...');
global.document = jsdom.window.document;
global.Node = jsdom.window.Node;
global.Event = jsdom.window.Event;


{ // test on basic element
  const div = Util.create('div', {
    classList: ['class-a', 'class-b'],
    visible: true,
  }, 'testContent');
  assert.equal(div.innerHTML, 'testContent');
  assert.equal(div.className, 'class-a class-b');
  assert.ok(div.getAttribute('visible'));

  const nestedDiv = Util.create('div', {}, Util.create('div'));
  assert.equal(nestedDiv.outerHTML, '<div><div></div></div>');
}

{ // test on functional component
  function MockFunctionalComponent(props) {
    return Util.create('div', {},
      Util.create('span', {}, props.label),
      Util.create('span', {}, props.content),
    );
  }

  const mockResult = '<div><span>label</span><span>content</span></div>';

  assert.equal(Util.create(MockFunctionalComponent, {
    label: 'label',
    content: 'content',
  }).outerHTML, mockResult, 'MockFunctionalComponent test failed');
}

{ // test on object-component
  const MockComponent = {
    base: 'div',

    init(props) {
      this.labelSpan = Util.create('span', { target: this });
      this.textSpan = Util.create('span', { target: this });
      Object.assign(this, props);
    },

    label: {
      set(label) { this.labelSpan.textContent = label; },
      get() { return this.labelSpan.textContent; },
    },

    content: {
      set(content) { this.textSpan.textContent = content; },
      get() { return this.textSpan.textContent; },
    },
  };

  const mockResult = '<div><span>label</span><span>text</span></div>';

  assert.equal(
    Util.create(MockComponent, {
      label: 'label',
      content: 'text',
    }).outerHTML,
    mockResult,
    'MockComponent test failed',
  );
}

{ // test on component using complex new-initializer
  const MockComponentNew = {
    base() {
      return Util.create('div', { className: 'new-component' });
    },

    init() {
      this.textContent = 'test';
    },
  };

  assert.equal(
    Util.create(MockComponentNew).outerHTML,
    '<div class="new-component">test</div>',
  );
}

{ // tests for helpers
  const e = Util.create('div');
  e.hide();
  assert.equal(e.style.display, 'none');
  e.show();
  assert.equal(e.style.display, '');
  e.classList.add('someclass');
  e.innerHTML = 'abc';
  e.clear();
  assert.equal(e.className, 'someclass');
  assert.equal(e.innerHTML, '');
  e.data.a = 'b';
  assert.equal(e.getAttribute('data-a'), 'b');
  assert.equal(e.data.a, 'b');
  e.data.set({ a: 'c' });
  assert.equal(e.data.a, 'c');

  const div = Util.create('div');
  e.content = div;
  assert.equal(e.innerHTML, '<div></div>');
  assert.equal(e.content[0], div);
}

{ // test for get()
  document.body.innerHTML = '<div class="test">test</div>';
  const div = Util.get('.test');
  assert.equal(div.textContent, 'test');
  div.content = 'changed';
  assert.equal(div.textContent, 'changed');
}

{ // test for trait
  const Vehicle = {
    move() {
      this.position += 1;
    },
    init() {
      this.isVehicle = true;
    },
  };

  const Child = {
    base: 'div',
    uses: Vehicle,
    position: 0,
    init() {
      this.isChild = true;
    },
  };

  const child = Util.create(Child);
  child.move();
  assert.equal(child.position, 1);
  assert.ok(child.isVehicle);
  assert.ok(child.isChild);
}

{ // test for ref
  let ref;
  const element = Util.create('div', { ref: (e) => { ref = e; } });

  assert.equal(element, ref);
}

{ // test for Array-components
  const Fragment = {
    base: () => [],
    init() {
      this.push(Util.create('div', {}, 'innerContent'));
    },
  };

  const fragment = Util.create(Fragment);
  const div = Util.create('div', {}, fragment);

  assert.equal(div.innerHTML, '<div>innerContent</div>');
}

{ // test for attach-event
  const root = Util.get('body');
  const div = Util.create('div', { target: root });

  assert.ok(div.isAttached);
  root.removeChild(div);
  assert.ok(!div.isAttached);
  root.appendChild(div);
  assert.ok(div.isAttached);
  root.clear();
  assert.ok(!div.isAttached);
}

{ // test for default properties
  const Component = {
    base: 'div',
    prop: {
      get() {
        return this.textContent;
      },
      set(text) {
        this.textContent = text;
      },
      default: 'text',
    },
  };
  const div = Util.create(Component);
  assert.equal(div.textContent, 'text');
  assert.equal(div.default.prop, 'text');
}

{ // test for multiple inheritance
  const BaseA = {
    base: 'div',
    name: 'a',
    init() {
      this.a = 'a';
    },
  };

  const BaseB = {
    name: 'b',
    init() {
      this.b = 'b';
    },
  };

  const Derived = {
    uses: [BaseA, BaseB],
  };

  const div = Util.create(Derived);
  assert.equal(div.a, 'a');
  assert.equal(div.b, 'b');
}

{ // test for nested inheritance
  const BaseA = {
    base: 'div',
    init() {
      this.a = 'a';
    },
  };

  const BaseB = {
    uses: BaseA,
    init() {
      this.b = 'b';
    },
  };

  const Derived = {
    uses: BaseB,
  };

  const div = Util.create(Derived);
  assert.equal(div.a, 'a');
  assert.equal(div.b, 'b');
}

{ // test set content
  const div = Util.create('div');
  const span = Util.create('span');
  const text = 'abcxy';

  div.content = span;
  assert.equal(div.innerHTML, '<span></span>');
  div.content = [span, text];
  assert.equal(div.innerHTML, '<span></span>abcxy');
  div.content = [text, span];
  assert.equal(div.innerHTML, 'abcxy<span></span>');
}

{ // test textContent prop
  const div = Util.create('div', { textContent: 'test' });
  assert.equal(div.innerHTML, 'test');
}

console.log('OK!');
