module.exports = [{
  entry: ['./src.js'],

  mode: 'production',

  output: {
    filename: 'domore.min.js',
    library: 'DOMore',
    path: __dirname,
  },
}, {
  entry: [
    'babel-polyfill',
    './src.js',
  ],

  mode: 'production',

  module: {
    rules: [{
      test: /\.js$/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [['@babel/preset-env', {
            targets: '> 0.25%, not dead',
            useBuiltIns: 'entry',
          }]],
        },
      },
    }],
  },

  output: {
    filename: 'domore.compat.min.js',
    library: 'DOMore',
    path: __dirname,
  },
}];
