/**
 * @module Util
 * @author Michel Smola
 * a sane DOM API
 */

/**
 * flatten array of children
 * @private
 */
const parseChildren = children => [].concat(...([].concat(...children)))
  .filter(child => child !== null)
  .map(child => (typeof child === 'object' ? child : exports.text(child)));

/**
 * Update content of an element. Insert new Nodes, move existing ones, remove old ones.
 * @private
 */
const updateContent = (element, children) => {
  let current = element.firstChild;
  parseChildren(children).forEach((child) => {
    // append at end if no more old children
    if (!current) {
      element.appendChild(child);
      return;
    }
    // if element is the same move on to next element
    if (current === child) {
      current = current.nextSibling;
      return;
    }
    // insert before element if not the same
    element.insertBefore(child, current);
  });
  // remove nodes that were not in children
  if (current) {
    while (current) {
      const next = current.nextSibling;
      element.removeChild(current);
      current = next;
    }
  }
};


/**
 * assign properties and methods to an element
 * @private
 */
const assign = (element, methods, options = null) => {
  const entries = options && options.ignore
    ? Object.entries(methods).filter(([name]) => !options.ignore.includes(name))
    : Object.entries(methods);
  entries.forEach(([name, method]) => {
    switch (typeof method) {
      case 'function':
        // eslint-disable-next-line no-param-reassign
        element[name] = method.bind(element);
        return;
      case 'object':
        if (method && (method.get || method.set)) {
          Object.defineProperty(element, name, Object.assign({ configurable: true }, method));
          if (method.default) {
            if (!element.default) element.default = {};
            element.default[name] = method.default;
          }
          return;
        }
      // eslint-disable-next-line no-fallthrough
      default:
        element[name] = method;
    }
  });
  return element;
};

const snakeCase = str => str
  .replace(/([a-z])([A-Z])/g, '$1-$2')
  .replace(/([a-zA-Z])([A-Z][a-z])/g, '$1-$2')
  .toLowerCase();


/**
 * jQuery style Syntactic sugar.
 * Components created with `Util.create` or retrieved with `Util.get` have some
 * useful helper functions attached.
 * @namespace helpers
 */
const helpers = {
  /**
   * @member
   * @instance
   * @type {bool}
   */
  isAttached: false,
  /** appendChild with lifecycle callbacks */
  appendChild(child) {
    child = Node.prototype.appendChild.call(this, child);
    if (this.isAttached && child && child._onattach) return child._onattach();
    return child;
  },
  /** removeChild with lifecycle callbacks */
  removeChild(child) {
    child = Node.prototype.removeChild.call(this, child);
    if (this.isAttached && child && child._ondetach) return child._ondetach();
    return child;
  },
  /** append elements and call lifecycle callbacks */
  append(...children) {
    return parseChildren(children).map(child => this.appendChild(child));
  },
  /** remove elements and call lifecycle callbacks */
  remove(...children) {
    return [].concat(...children).map(child => this.removeChild(child));
  },
  /** insertAdjacentElement with lifecycle callbacks */
  insertAdjacentElement(position, element) {
    element = Element.prototype.insertAdjacentElement.call(this, position, element);
    if (this.isAttached && element && element._onattach) return element._onattach();
    return element;
  },
  /** insertBefore with lifecycle callbacks */
  insertBefore(element, before) {
    element = Node.prototype.insertBefore.call(this, element, before);
    if (this.isAttached && element && element._onattach) return element._onattach();
    return element;
  },
  /** removes all children */
  clear() {
    this.childNodes.forEach((element) => { if (element._ondetach) element._ondetach(); });
    this.innerHTML = '';
    return this;
  },
  _onattach() {
    if (!this.isAttached) {
      this.isAttached = true;
      this.dispatchEvent(new Event('attach'));
      this.childNodes.forEach((element) => { if (element._onattach) element._onattach(); });
    }
    return this;
  },
  _ondetach() {
    if (this.isAttached) {
      this.isAttached = false;
      this.dispatchEvent(new Event('detach'));
      if (this.cancelClickOutside) this.cancelClickOutside();
      this.childNodes.forEach((element) => { if (element._ondetach) element._ondetach(); });
    }
    return this;
  },

  /** shows the element (`style.display = ''`) */
  show() {
    this.style.display = '';
    return this;
  },
  /** hides the element (`style.display = 'none'`) */
  hide() {
    this.style.display = 'none';
    return this;
  },
  /**
   * Comfortably set <code>'data-'</code>-attributes. Use either:
   * <ul>
   * <li><code>node.data.foo = 'bar'</code></li>
   * <li><code>node.data.set({ foo: 'bar'})</code></li>
   * <li><code>node.data.foo // bar</code></li>
   * <li><code>node.data.get('foo') // bar</code></li>
   * </ul>
   * Attribute name may be specified in snake-case or camelCase, it is converted to snake-case either way.
   * @member
   * @instance
   * @type {object}
   */
  data: {
    get() {
      return new Proxy(this, {
        get(obj, attr) {
          switch (attr) {
            case 'set':
              return def => Object.entries(def)
                .forEach(([name, value]) => obj.setAttribute(`data-${snakeCase(name)}`, value));
            case 'get':
              return name => obj.getAttribute(`data-${snakeCase(name)}`);
            default:
              return obj.getAttribute(`data-${snakeCase(attr)}`);
          }
        },
        set(obj, attr, value) {
          if (value === false || value === null) {
            obj.removeAttribute(`data-${snakeCase(attr)}`);
          } else {
            obj.setAttribute(`data-${snakeCase(attr)}`, value);
          }
          return true;
        },
      });
    },
  },
  /**
   * Comfortably set `'aria-'`-attributes. Use either:
   * <ul>
   * <li><code>node.aria.foo = 'bar'</code></li>
   * <li><code>node.aria.set({ foo: 'bar'})</code></li>
   * <li><code>node.aria.foo // bar</code></li>
   * <li><code>node.aria.get('foo') // bar</code></li>
   * </ul>
   * Attribute name may be specified in snake-case or camelCase, it is converted to snake-case either way.
   * @member
   * @instance
   * @type {object}
   */
  aria: {
    get() {
      return new Proxy(this, {
        get(obj, attr) {
          switch (attr) {
            case 'set':
              return def => Object.entries(def)
                .forEach(([name, value]) => obj.setAttribute(`aria-${snakeCase(name)}`, value));
            case 'get':
              return name => obj.getAttribute(`aria-${snakeCase(name)}`);
            default:
              return obj.getAttribute(`aria-${snakeCase(attr)}`);
          }
        },
        set(obj, attr, value) {
          if (value === false || value === null) {
            obj.removeAttribute(`aria-${snakeCase(attr)}`);
          } else {
            obj.setAttribute(`aria-${snakeCase(attr)}`, value);
          }
          return true;
        },
      });
    },
  },
  /**
   * Comfortably replace and node's children. Will eat and return arrays.
   * @member
   * @instance
   * @type {HTMLElement[]|HTMLElement}
   */
  content: {
    get() {
      return Array.from(this.childNodes);
    },
    set(content) {
      if (Array.isArray(content)) {
        updateContent(this, content);
      } else {
        updateContent(this, [content]);
      }
    },
  },
  /**
   * call a callback exactly once the next time the user klicks outside the element
   * @method
   * @instance
   * @param {function} callback
   */
  handleClickOutside(callback) {
    const handleClick = (event) => {
      if (this === event.target || this.contains(event.target)) return;
      callback();
      window.removeEventListener('click', handleClick);
    };
    window.addEventListener('click', handleClick);
    this.cancelClickOutside = () => {
      window.removeEventListener('click', handleClick);
      this.cancelClickOutside = undefined;
    };
  },
};

/**
 * Wrapper for document.createTextNode
 * @param {string} content
 * @return {Node}
 */
exports.text = content => document.createTextNode(content);

/**
 * @private
 * return a flattened array containing all ancestors of component followed by component itselft
 * @param {Component} component
 * @return {Array<Component>}
 */
const disassemble = component => (
  // eslint-disable-next-line no-nested-ternary
  component.uses ? (
    Array.isArray(component.uses)
      ? [].concat(...component.uses.map(disassemble), component)
      : [].concat(disassemble(component.uses), component)
  ) : [component]
);

/**
 * <b>This is where the magic happens.</b>
 * <p>Creates composable elements to work with the HTML-DOM in a sane way and attaches {@link helpers} to them..</p>
 * <p>Syntax is purposefully kept similar to react except without fancy jsx.</p>
 * <ul>
 * <li>A simple wrapper around document.createElement if component is a string.</li>
 * <li>Produces composite elements when component is a function.</li>
 * <li>Produces dynamic, class-instance-like elements with methods and such if component is an object.</li>
 * </ul>
 *
 * @param {(string|object|function)} component defines what kind of component we want to create.
 * @param {object} props properties that translate into HTML-attributes in most cases
 * @param {...HTMLElement[]} children children that will be appended as content to the created node
 */
exports.create = (component, props = {}, ...children) => {
  if (props === null) props = {};
  if (props.className && props.classList) {
    props.classList = props.classList.concat(props.className.split(' ').filter(c => c !== ''));
  } else if (props.className) {
    props.classList = props.className.split(' ').filter(c => c !== '');
  }
  // create the HTMLElement
  let e;
  switch (typeof component) {
    // functional / static
    case 'function':
      e = component(props, parseChildren(children));
      break;
    // object / dynamic
    case 'object':
      {
        const parts = disassemble(component);
        if (parts.find(part => parts.filter(filterPart => filterPart === part).length > 1)) {
          throw new Error('Component cannot use the same component twice.');
        }
        const composition = Object.assign({}, helpers, ...parts);
        children = parseChildren(children);

        e = typeof composition.base === 'function'
          ? composition.base(props, children)
          : document.createElement(composition.base);

        assign(e, composition, { ignore: ['base', 'init', 'uses'] });

        parts.forEach((part) => {
          if (part.init) part.init.call(e, props, children);
        });
        // assign default props
        if ('default' in e) Object.entries(e.default).forEach(([prop, value]) => { e[prop] = value; });
      }
      break;
    // simple
    case 'string':
      e = document.createElement(component);
      assign(e, helpers, { ignore: props.override });
      // className shorthand
      if (typeof props === 'string') {
        e.className = props;
      // assign props
      } else {
        Object.entries(props).forEach(([name, value]) => {
          switch (name) {
            // pass on properties
            case 'innerHTML':
            case 'textContent':
            case 'id':
            case 'tabIndex':
              if (value !== null) e[name] = value;
              return;
            // set classList
            case 'classList':
              e.classList.add(...value.filter(v => v !== null && v !== ''));
              return;
            // set style
            case 'style':
              Object.assign(e.style, value);
              return;
            // shorthand for data
            case 'data':
              e.data.set(value);
              return;
            case 'aria':
              e.aria.set(value);
              return;
            // ignore
            case 'target':
            case 'placement':
            case 'before':
            case 'override':
            case 'className':
              return;
            default: // trickier conditions
              if (name.startsWith('on')) {
                // eventListeners
                if (typeof value === 'function') e.addEventListener(name.slice(2).toLowerCase(), value);
                // standard attributes
              } else if (value !== false) {
                e.setAttribute(snakeCase(name), value);
              }
          }
        });
      }
      // append children
      if (children) e.append(children);
      break;
    default:
      return null;
  }
  // append to target
  if (props.target) {
    if (props.before) {
      props.target.insertBefore(e, props.before);
    } else if (props.placement) {
      props.target.insertAdjacentElement(props.placement, e);
    } else {
      props.target.appendChild(e);
    }
  }

  if (props.ref) props.ref(e);

  return e;
};

/**
 * Wrapper around querySelectorAll that throws the result in an array only if it has more than one entry, returns null
 * if no results and also attaches helper-functions to all results.
 * @param {string} selector
 * @return {(null|HTMLElement|HTMLElement[])}
 */
exports.get = (selector) => {
  const results = Array.from(document.querySelectorAll(selector));
  switch (results.length) {
    case 0:
      return null;
    case 1:
      if (results[0].show) return results[0]._onattach();
      return assign(results[0], helpers)._onattach();
    default:
      return results.map((result) => {
        if (result.show) return result._onattach();
        return assign(result, helpers)._onattach();
      });
  }
};
