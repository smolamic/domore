# DOMore
[![pipeline status](https://gitlab.com/smolamic/domore/badges/master/pipeline.svg)](https://gitlab.com/smolamic/domore/commits/master)

do more with the standard DOM-API

## What is it?
+ A wrapper around the standard javascript DOM-API for better syntax
+ A couple of practical extra events
+ A method to extend standard HTMLElements

## Why choose DOMore?
it is:

### small
6kB minified, 2.1kB minified and gzipped.

### easy to integrate
Basically just a small extension to the standard DOM-API.

### composable
Create reusable components in a functional or class-like syntax similar to react.

### low overhead
No shadow-DOM, nothing unnecessary, just syntactic sugar that lets you
extend built-in DOM-classes.

### JSX-compatible
You can use the react-jsx precompiler to use jsx-syntax.

## Documentation
Is in the [Wiki](https://gitlab.com/smolamic/domore/wikis/home)
